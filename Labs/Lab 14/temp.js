var weather = [
    {date:"NOV 20", day: "MONDAY", hi: 80, lo: 55 },
    { date:"NOV 21", day: "TUESDAY", hi: 75, lo: 52 },
    {date:"NOV 22",  day: "WEDNESDAY", hi: 69, lo: 52 },
    { date:"NOV 23", day: "THURSDAY", hi: 69, lo: 48 },
    {date:"NOV 24",  day: "FRIDAY", hi: 68, lo: 51 }
  ];
  
  var avg0 = (weather[0]["hi"] + weather[0]["lo"])/2
  var avg1 = (weather[1]["hi"] + weather[1]["lo"])/2
  var avg2 = (weather[2]["hi"] + weather[2]["lo"])/2
  var avg3 = (weather[3]["hi"] + weather[3]["lo"])/2
  var avg4 = (weather[4]["hi"] + weather[4]["lo"])/2
  
  var el = document.getElementById("table_data");
  el.innerHTML = "<th>Date</th><th>Weekday</th> <th>Temp (Hi)</th> <th>Temp (Lo)</th> <th>Temp Avg</th>";
  
  var i = 0
  el.innerHTML +=
    "<tr><td>" + weather[i]["date"] +
    "</td><td>" + weather[i]["day"] +
    "</td><td>"+ weather[i]["hi"]+"&deg"+
    "</td><td>"+ weather[i]["lo"]+"&deg" +
    "</td><td>"+ avg0 +"&deg"+ "</td><tr>";
  
  var i = 1
  el.innerHTML +=
   "<tr><td>" + weather[i]["date"]+
    "</td><td>" + weather[i]["day"] +
    "</td><td>"+ weather[i]["hi"] +"&deg"+
    "</td><td>"+ weather[i]["lo"] +"&deg"+
    "</td><td>"+ avg1 + "&deg"+"</td><tr>";
  
  var i = 2
  el.innerHTML +=
    "<tr><td>" + weather[i]["date"]+
    "</td><td>" + weather[i]["day"] +
    "</td><td>"+ weather[i]["hi"] +"&deg"+
    "</td><td>"+ weather[i]["lo"] +"&deg"+
    "</td><td>"+ avg2 +"&deg"+ "</td><tr>";
  
  var i = 3
  el.innerHTML +=
    "<tr><td>" + weather[i]["date"]+
    "</td><td>" + weather[i]["day"] +
    "</td><td>"+ weather[i]["hi"] +"&deg"+
    "</td><td>"+ weather[i]["lo"]+"&deg" +
    "</td><td>"+ avg3 +"&deg"+ "</td><tr>";
  
  var i = 4
  el.innerHTML +=
    "<tr><td>" + weather[i]["date"]+
    "</td><td>" + weather[i]["day"] +
    "</td><td>"+ weather[i]["hi"] +"&deg"+
    "</td><td>"+ weather[i]["lo"] +"&deg"+
    "</td><td>"+ avg4 +"&deg"+ "</td><tr>";
  
  var high = [80,75,69,69,68];
  var low = [55,52,52,48,51];
  
  var el1 = document.getElementById('temp1');
  var el2 = document.getElementById('temp2');
  
  function getSum(total, num){
    return total + num;
  }
  
  el1.innerHTML = "High:"+ " " + high.reduce(getSum) / high.length + "&deg;Degrees";
  el2.innerHTML = "Low:"+ " "+ low.reduce(getSum) / low.length  +"&deg;Degrees";
  
  
  
  