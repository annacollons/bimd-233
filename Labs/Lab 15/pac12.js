var washington = document.getElementById('one');
var washingtonState = document.getElementById('two');
var stanford = document.getElementById('three');
var california = document.getElementById('four');
var oregon = document.getElementById('five');
var oregonState = document.getElementById('six');

function fill()
{
  california.id = "cal";
  oregon.id = "ou";
  oregonState.id = "osu";
  stanford.id = "su";
  washington.id = "uw";
  washingtonState.id = "wsu";
  
  california.getElementsByClassName('college')[0].innerHTML = "<b>California</b>";
  oregon.getElementsByClassName('college')[0].innerHTML = "<b>Oregon</b>";
  oregonState.getElementsByClassName('college')[0].innerHTML = "<b>Oregon State</b>";
  stanford.getElementsByClassName('college')[0].innerHTML = "<b>Stanford</b>";
  washington.getElementsByClassName('college')[0].innerHTML = "<b>Washington</b>";
  washingtonState.getElementsByClassName('college')[0].innerHTML = "<b>Washington State</b>";
  
  california.getElementsByClassName('conference')[0].innerHTML = "<b>2-6</b>";
  oregon.getElementsByClassName('conference')[0].innerHTML = "<b>2-6</b>";
  oregonState.getElementsByClassName('conference')[0].innerHTML = "<b>2-6</b>";
  stanford.getElementsByClassName('conference')[0].innerHTML = "<b>6-3</b>";
  washington.getElementsByClassName('conference')[0].innerHTML = "<b>7-1</b>";
  washingtonState.getElementsByClassName('conference')[0].innerHTML = "<b>7-1</b>";
  
  california.getElementsByClassName('overall')[0].innerHTML = "<b>4-7</b>";
  oregon.getElementsByClassName('overall')[0].innerHTML = "<b>4-7</b>";
  oregonState.getElementsByClassName('overall')[0].innerHTML = "<b>3-8</b>";
  stanford.getElementsByClassName('overall')[0].innerHTML = "<b>8-3</b>";
  washington.getElementsByClassName('overall')[0].innerHTML = "<b>10-1</b>";
  washingtonState.getElementsByClassName('overall')[0].innerHTML = "<b>8-3</b>";

}

