var radiusOne = 32;
var radiusTwo = 66;
var radiusThree = 104;

function calcCircleGeometries(radius) {
  const pi = Math.PI;
  var area = pi * radius * radius;
  var circumference = 2 * pi * radius;
  var diameter = 2 * radius;
  var geometries = [area, circumference, diameter];
  return (
    "<tr><td>" +
    radius +
    "</td>" +
    "<td>" +
    area +
    "</td>" +
    "<td>" +
    circumference +
    "</td>" +
    "<td>" +
    diameter +
    "</td></tr>"
  );
}

document.getElementById("radiusOne").innerHTML = calcCircleGeometries(
  radiusOne
);
document.getElementById("radiusTwo").innerHTML = calcCircleGeometries(
  radiusTwo
);
document.getElementById("radiusThree").innerHTML = calcCircleGeometries(
  radiusThree
);
